import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose';
import { IProduct } from './interfaces/product.interface';
import { CreateProductDTO } from './dto/product.dto';

@Injectable()
export class ProductService {

    constructor(@InjectModel('Product') readonly productModel: Model<IProduct>){}

    async getProducts(): Promise<IProduct[]>{
        const productList: IProduct[] = await this.productModel.find()
        return productList
    }
    async getProduct(productId: string):Promise<IProduct> {
        const product: IProduct = await this.productModel.findById(productId)
        return product
    }
    async createProduct(createProductDTO: CreateProductDTO): Promise<IProduct>{
        const productCreated = new this.productModel(createProductDTO)
        return await productCreated.save()
    }
    async deleteProduct(productId: string): Promise<IProduct>{
        const deletedProduct = await this.productModel.findByIdAndDelete(productId)
        return deletedProduct
    }
    async updateProduct(productId: string, createdProductDTO: CreateProductDTO): Promise<IProduct> {
         const productUpdated = await this.productModel.findByIdAndUpdate(productId, createdProductDTO, {
             new: true
            })
         return productUpdated   
    }

}

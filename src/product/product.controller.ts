import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param } from '@nestjs/common';
import { Response } from 'express';
import { CreateProductDTO } from './dto/product.dto';
import { ProductService } from './product.service';


@Controller('product')
export class ProductController {

    constructor (private readonly productService: ProductService) {}

    @Get('/')
    async getProducts (@Res() res: Response) {
       const products =  await this.productService.getProducts()
        return res.status(HttpStatus.OK).json({
            message: 'Product list',
            'products List': products
        })          
    }
    @Get('/:productId') 
    async getProductById (@Param('productId') productId: string, @Res() res: Response) {
        const product =  await this.productService.getProduct(productId)
        return res.status(HttpStatus.FOUND).json({
            message: 'List a Product by Id',
            product
        })        
    }
    @Put('/:productId')
    async updateProductById (@Body() createProductDTO: CreateProductDTO, @Param('productId') productId: string, @Res() res: Response) {
        const productUpdated = await this.productService.updateProduct(productId,createProductDTO)
        return res.status(HttpStatus.OK).json({
            message: 'Product Updated',
            product: productUpdated
        })
    }
    @Post('/create')
    async createProduct(@Body() createProductDTO: CreateProductDTO, @Res() res: Response ){
        const productCreated = await this.productService.createProduct(createProductDTO)
        return res.status(HttpStatus.CREATED).json({
            message: 'Product created',
            product: productCreated
        })
    }
}
